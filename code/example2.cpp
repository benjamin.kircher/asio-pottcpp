// two asynchronous operations with steady_timer

#include <boost/asio/io_service.hpp>
#include <boost/asio/steady_timer.hpp>

#include <chrono>
#include <iostream>

using namespace boost::asio;

int main() {
  io_service service;
  steady_timer timer1(service, std::chrono::seconds(10));
  timer1.async_wait([](const auto &error_code) {
    std::cout << "10 sec\n";
  });
  steady_timer timer2(service, std::chrono::seconds(3));
  timer2.async_wait([](const auto &error_code) {
    std::cout << "3 sec\n";
  });
  service.run();
  return 0;
}
