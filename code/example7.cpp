// Same time server with coroutines
#include <boost/asio/buffer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/asio/write.hpp>

#include <string>
#include <vector>

#include <time.h>

using namespace boost::asio;
using namespace boost::asio::ip;

io_service service;
tcp::endpoint tcp_endpoint(tcp::v6(), 8080);
tcp::acceptor tcp_acceptor(service, tcp_endpoint);
std::vector<tcp::socket> sockets;

void do_write(tcp::socket &socket, yield_context yield) {
  auto now = time(nullptr);
  std::string data = ctime(&now);
  async_write(socket, buffer(data), yield);
  socket.shutdown(tcp::socket::shutdown_send);
}

void do_accept(yield_context yield) {
  for (int i = 0; i < 2; ++i) {
    sockets.emplace_back(tcp::socket(service));
    tcp_acceptor.async_accept(sockets.back(), yield);
    spawn(service, [](yield_context yield) {
      do_write(sockets.back(), yield);
    });
  }
}

int main() {
  tcp_acceptor.listen();
  spawn(service, do_accept);
  service.run();
  return 0;
}
