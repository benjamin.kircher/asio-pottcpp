// multiple threads - multiple IO services
#include <boost/asio/io_service.hpp>
#include <boost/asio/steady_timer.hpp>
#include <chrono>
#include <iostream>
#include <thread>
using namespace boost::asio;
int main() {
  io_service service1;
  io_service service2;
  steady_timer timer1(service1, std::chrono::seconds(10));
  timer1.async_wait([](const auto &error_code) {
    std::cout << "10 sec\n";
  });
  steady_timer timer2(service2, std::chrono::seconds(3));
  timer2.async_wait([](const auto &error_code) {
    std::cout << "3 sec\n";
  });
  std::thread thread1([&] { service1.run(); });
  std::thread thread2([&] { service2.run(); });
  thread1.join();
  thread2.join();
  return 0;
}
