// A web client
#include <boost/asio/buffer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/write.hpp>

#include <array>
#include <iostream>
#include <string>

#include <stdlib.h>

using namespace boost::asio;
using namespace boost::asio::ip;

io_service service;
tcp::resolver resolv(service);
tcp::socket tcp_socket(service);
std::array<char, 4096> bytes;

void response_handler(const boost::system::error_code &ec,
                      size_t bytes_received) {
  if (!ec) {
    std::cout.write(bytes.data(), bytes_received);
    tcp_socket.async_read_some(buffer(bytes),
                               response_handler);
  }
}

void connect_handler(const boost::system::error_code &ec) {
  if (!ec) {
    std::string req =
        "GET / HTTP/1.1\r\nHost: tagesschau.de\r\n\r\n";
    write(tcp_socket, buffer(req));
    tcp_socket.async_read_some(buffer(bytes),
                               response_handler);
  }
}

void resolve_handler(const boost::system::error_code &ec,
                     tcp::resolver::iterator it) {
  if (!ec)
    tcp_socket.async_connect(*it, connect_handler);
}

int main() {
  tcp::resolver::query query("tagesschau.de", "80");
  resolv.async_resolve(query, resolve_handler);
  service.run();
  return 0;
}
