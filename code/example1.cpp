// boost::asio::steady_timer in action

#include <boost/asio/io_service.hpp>
#include <boost/asio/steady_timer.hpp>

#include <chrono>
#include <iostream>

using namespace boost::asio;

int main() {
  io_service service;
  steady_timer timer(service, std::chrono::seconds(10));
  timer.async_wait([](const auto &error_code) {
    std::cout << "10 sec\n";
  });
  service.run();
  return 0;
}
