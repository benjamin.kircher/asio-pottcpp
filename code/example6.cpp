// A time server with ip::tcp::acceptor
#include <boost/asio/buffer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/write.hpp>

#include <string>

#include <time.h>

using namespace boost::asio;
using namespace boost::asio::ip;

io_service service;
tcp::endpoint tcp_endpoint(tcp::v6(), 8080);
tcp::acceptor tcp_acceptor(service, tcp_endpoint);
tcp::socket tcp_socket(service);
std::string data;

void write_handler(const boost::system::error_code &ec,
                   size_t bytes_transferred) {
  if (!ec) {
    tcp_socket.shutdown(tcp::socket::shutdown_send);
  }
}

void accept_handler(const boost::system::error_code &ec) {
  if (!ec) {
    auto now = time(nullptr);
    data = ctime(&now);
    async_write(tcp_socket, buffer(data), write_handler);
  }
}

int main() {
  tcp_acceptor.listen();
  tcp_acceptor.async_accept(tcp_socket, accept_handler);
  service.run();
  return 0;
}
